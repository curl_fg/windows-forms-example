﻿namespace CryptoCurrencies
{
    partial class MainWin
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            FlowLayout = new FlowLayoutPanel();
            GetBtn = new Button();
            FlowLayout.SuspendLayout();
            SuspendLayout();
            // 
            // FlowLayout
            // 
            FlowLayout.AutoScroll = true;
            FlowLayout.Controls.Add(GetBtn);
            FlowLayout.Dock = DockStyle.Fill;
            FlowLayout.Location = new Point(0, 0);
            FlowLayout.Margin = new Padding(0);
            FlowLayout.Name = "FlowLayout";
            FlowLayout.Padding = new Padding(10);
            FlowLayout.Size = new Size(709, 461);
            FlowLayout.TabIndex = 0;
            // 
            // GetBtn
            // 
            GetBtn.Location = new Point(20, 20);
            GetBtn.Margin = new Padding(10);
            GetBtn.Name = "GetBtn";
            GetBtn.Size = new Size(670, 29);
            GetBtn.TabIndex = 0;
            GetBtn.Text = "Obtener datos";
            GetBtn.UseVisualStyleBackColor = true;
            GetBtn.Click += OnClick;
            // 
            // MainWin
            // 
            ClientSize = new Size(709, 461);
            Controls.Add(FlowLayout);
            MinimumSize = new Size(550, 500);
            Name = "MainWin";
            Text = "App";
            Resize += MainWInResize;
            FlowLayout.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private FlowLayoutPanel FlowLayout;
        private Button GetBtn;
    }
}