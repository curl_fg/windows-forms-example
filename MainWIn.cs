using System.Collections.Specialized;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Web;

// TODO: Adjust controls size when maximize/minimize action is executed
namespace CryptoCurrencies
{
    public partial class MainWin : Form
    {
        private int CurrentPage = 1;

        private List<Currency> WillBoxes = [];

        private NameValueCollection QueryParams = HttpUtility.ParseQueryString(
            "vs_currency=usd&per_page=10&order=price_change_percentage_24h&locale=es"
        );

        private readonly HttpClient Http =
            new() { BaseAddress = new Uri("https://api.coingecko.com/api/v3/") };

        private readonly string ListEndpoint = "coins/markets";

        [GeneratedRegex(@"^\s*$")]
        private static partial Regex BlankOrSpaces();

        public MainWin()
        {
            Http.DefaultRequestHeaders.Add(
                "User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36"
            );

            InitializeComponent();
        }

        private void SwitchAllBtn(bool enabled)
        {
            foreach (var item in FlowLayout.Controls.OfType<Button>())
                item.Enabled = enabled;
        }

        private Task<string> GetList()
        {
            QueryParams["page"] = $"{++CurrentPage}";
            return Http.GetStringAsync($"{ListEndpoint}?{QueryParams}");
        }

        // public void HelloWorld() => Console.WriteLine("Hello world!");

        private async void OnClick(object? sender, EventArgs e)
        {
            // Type t = GetType();
            // var method = t.GetMethod("HelloWorld");
            // method?.Invoke(this, []);

            SwitchAllBtn(false);

            try
            {
                var data = await GetList();
                var currencies = JsonSerializer.Deserialize<List<Currency>>(data) ?? [];

                WillBoxes = WillBoxes
                    .Concat(currencies)
                    .OrderBy((coin) => coin.price_change_percentage_24h)
                    .ToList();

                AddCtrls(currencies);
            }
            catch (HttpRequestException err)
            {
                Console.WriteLine(err);
            }
            finally
            {
                FlowLayout
                    .Controls.OfType<GroupBox>()
                    .ToList()
                    .ForEach(
                        (ctrl) =>
                            FlowLayout.Controls.SetChildIndex(
                                (Control)ctrl,
                                WillBoxes.FindIndex(
                                    (c) => $"CoinGroup{c.symbol}" == ((Control)ctrl).Name
                                ) + 1
                            )
                    );

                SwitchAllBtn(true);
                PerformLayout();
            }
        }

        private void AddCtrls(List<Currency> currencies)
        {
            // FlowLayout.SuspendLayout();
            SuspendLayout();

            foreach (var currency in currencies)
            {
                if (!BlankOrSpaces().IsMatch(currency.image))
                {
                    var req = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(Regex.Replace(currency.image, @"/large/", @"/small/")),
                    };

                    FlowLayout.Controls.Add(
                        GenGrp(
                            currency,
                            Bitmap.FromStream(Http.SendAsync(req).Result.Content.ReadAsStream())
                        )
                    );
                }
            }

            // FlowLayout.ResumeLayout(true);
            ResumeLayout(false);
        }

        private GroupBox GenGrp(Currency currency, Image img)
        {
            GroupBox CoinGroup = new();
            PictureBox CoinPic = new();
            Label Price = new();
            Label ChangeRate = new();

            CoinGroup.Controls.Add(ChangeRate);
            CoinGroup.Controls.Add(CoinPic);
            CoinGroup.Controls.Add(Price);
            CoinGroup.Location = new Point(10, 10);
            CoinGroup.Margin = new Padding(10);
            CoinGroup.Name = $"CoinGroup{currency.symbol}";
            CoinGroup.Padding = new Padding(10);
            CoinGroup.Size = new Size(145, 175);
            CoinGroup.TabIndex = 0;
            CoinGroup.TabStop = false;
            CoinGroup.Text = currency.name;

            ChangeRate.AutoSize = true;
            ChangeRate.Location = new Point(10, 140);
            ChangeRate.Margin = new Padding(0, 0, 0, 10);
            ChangeRate.Name = $"ChangeRate{currency.symbol}";
            ChangeRate.Size = new Size(59, 19);
            ChangeRate.TabIndex = 2;
            ChangeRate.Text = $"Change: {currency.price_change_percentage_24h}%";

            ChangeRate.ForeColor =
                currency.price_change_percentage_24h < 0 ? Color.Red : Color.Green;

            Price.AutoSize = true;
            Price.Location = new Point(10, 115);
            Price.Margin = new Padding(0, 0, 0, 10);
            Price.Name = $"Price{currency.symbol}";
            Price.Size = new Size(41, 19);
            Price.TabIndex = 1;
            Price.Text = $"Price: ${currency.current_price}";

            CoinPic.Image = img;
            CoinPic.Location = new Point(10, 30);
            CoinPic.Margin = new Padding(0, 0, 0, 10);
            CoinPic.Name = $"CoinPic{currency.symbol}";
            CoinPic.Size = new Size(125, 70);
            CoinPic.SizeMode = PictureBoxSizeMode.Zoom;
            CoinPic.TabIndex = 0;
            CoinPic.TabStop = false;

            return CoinGroup;
        }

        private void MainWInResize(object sender, EventArgs e)
        {
            GetBtn.Width =
                FlowLayout.Width
                - GetBtn.Margin.Left
                - GetBtn.Margin.Right
                - FlowLayout.Padding.Left
                - FlowLayout.Padding.Right
                - 15; // Scrollbar width
        }
    }

    class Currency
    {
        public required decimal current_price { get; set; }
        public required decimal price_change_percentage_24h { get; set; }
        public required string id { get; set; }
        public required string image { get; set; }
        public required string name { get; set; }
        public required string symbol { get; set; }
    }
}
